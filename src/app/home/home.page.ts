import { Component } from '@angular/core';
import { DndServerService } from '../services/dnd-server.service';
import { DndDatabaseService } from '../services/dnd-database.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {

    started = false;

    constructor(private dndService: DndServerService,
                private dndDatabase: DndDatabaseService) {
    }

    async startService(): Promise<void> {
        await this.dndService.start();
        this.started = true;
    }

    async stopService(): Promise<void> {
        await this.dndService.stop();
        this.started = false;
    }

}
