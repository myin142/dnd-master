export interface DndClass {
    name: string;
    hitDice: number;
}

export interface DndSubclass {
    name: string;
    class: string;
}

export interface Monster {
    id: number;
    name: string;
    armor: number;
    hitPoints: number;
    hitDice: string;
}

export interface Spell {
    id: number;
    name: string;
    desc: string;
    higherLevel: string;
    range: number;
    components: string;
    material: string;
    duration: string;
    castingTime: string;
    level: number;
}
