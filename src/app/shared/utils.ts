export class Utils {

    static moveInArray(arr: any[], from: number, to: number): void {
        const moveItem = arr[from];
        arr.splice(from, 1);
        arr.splice(to, 0, moveItem);
    }

}
