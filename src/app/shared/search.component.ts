import { EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';

export abstract class SearchComponent<T, R> implements OnChanges {

    @Input() search: R;
    @Input() skipFirst = true;
    @Output() clicked = new EventEmitter<T>();

    protected searchRequest = new Subject<R>();

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.search && (this.skipFirst || !changes.search.isFirstChange())) {
            this.searchRequest.next(changes.search.currentValue);
        }
    }

}
