import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { StorageService } from '../shared/storage.service';

@Injectable()
export class BattleService extends StorageService<Battle> {

    private static BATTLE_STATUS_KEY = 'battle-status-key';

    constructor(private storage: Storage) {
        super(storage, BattleService.BATTLE_STATUS_KEY);
    }

    sortByInitiative(): void {
        const players = [ ...this.getBattlePlayers() ];
        players.sort((a, b) => b.initiative - a.initiative);

        this.update({ players });
    }

    savePlayer(player: BattlePlayer): void {
        const currPlayers = this.getBattlePlayers();
        console.log('Save BattlePlayer', player);

        const playerIndex = currPlayers.findIndex(x => this.playerEqual(x, player));
        if (playerIndex !== -1) {
            currPlayers[playerIndex] = player;
        } else {
            currPlayers.push(player);
        }

        this.update({
            players: [ ...currPlayers ],
        });
    }

    findPlayer(player: BattlePlayer): BattlePlayer {
        return this.getBattlePlayers().find(x => this.playerEqual(x, player));
    }

    private playerEqual(player1: BattlePlayer, player2: BattlePlayer): boolean {
        return player1.name === player2.name;
    }

    deletePlayer(player: BattlePlayer): void {
        this.update({
            players: [
                ...this.getBattlePlayers().filter(x => !this.playerEqual(x, player)),
            ],
        });
    }

    getBattlePlayers(): BattlePlayer[] {
        return (this.value.value) ? this.value.value.players : [];
    }

}

export interface Battle {
    players: BattlePlayer[];
}

export interface BattlePlayer {
    name: string;
    initiative: number;
    armor: number;
    hitPoints: number;
    hitDice: number;
}
