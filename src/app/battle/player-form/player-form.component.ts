import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Monster } from '../../shared/dnd.model';
import { ModalController } from '@ionic/angular';
import { BattlePlayer, BattleService } from '../battle.service';

@Component({
    selector: 'app-player-form',
    templateUrl: './player-form.component.html',
})
export class PlayerFormComponent implements OnInit {

    @Input() data: Partial<BattlePlayer> = {};

    formGroup: FormGroup;
    monsters: Observable<Monster[]>;
    name: string;

    constructor(private builder: FormBuilder,
                private battleService: BattleService,
                private modal: ModalController) {
    }

    ngOnInit() {
        this.formGroup = this.builder.group({
            name: [{
                value: this.data.name,
                disabled: this.editMode,
            }, Validators.required],
            initiative: [this.data.initiative, Validators.required],
            armor: [this.data.armor],
            hitPoints: [this.data.hitPoints],
            hitDice: [this.data.hitDice],
        });

        this.formGroup.controls.name.valueChanges.subscribe(name => {
            this.name = name;
        });
    }

    get editMode(): boolean {
        return this.data.name != null;
    }

    preFillMonster(monster: Monster): void {
        this.formGroup.patchValue(monster);
    }

    submitData(): void {
        if (this.formGroup.invalid) {
            return;
        }

        const player: BattlePlayer = this.formGroup.getRawValue();

        if (!this.editMode && this.battleService.findPlayer(player) != null) {
            this.formGroup.controls.name.setErrors({ duplicate: true });
            return;
        }

        this.battleService.savePlayer(player);
        this.modal.dismiss();
    }

}
