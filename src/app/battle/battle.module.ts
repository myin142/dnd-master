import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BattlePage } from './battle.page';
import { BattleService } from './battle.service';
import { PlayerFormComponent } from './player-form/player-form.component';
import { MonsterModule } from '../modules/monster/monster.module';
import { MenuModule } from '../modules/menu/menu.module';

const routes: Routes = [
    {
        path: '',
        component: BattlePage,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReactiveFormsModule,
        MonsterModule,
        MenuModule,
        RouterModule.forChild(routes),
    ],
    declarations: [
        BattlePage,
        PlayerFormComponent,
    ],
    entryComponents: [
        PlayerFormComponent,
    ],
    providers: [
        BattleService,
    ],
})
export class BattlePageModule {
}
