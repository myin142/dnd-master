import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PlayerFormComponent } from './player-form/player-form.component';
import { Battle, BattlePlayer, BattleService } from './battle.service';
import { Utils } from '../shared/utils';

@Component({
    selector: 'app-battle',
    templateUrl: './battle.page.html',
    styleUrls: ['./battle.page.scss'],
})
export class BattlePage implements OnInit {

    battle: Battle;

    constructor(private modal: ModalController,
                private battleService: BattleService) {
    }

    ngOnInit() {
        this.battleService.get().subscribe(battle => {
            this.battle = battle;
        });
    }

    async openPlayerForm(player?: BattlePlayer): Promise<void> {
        const modalSettings = {
            component: PlayerFormComponent,
            componentProps: {},
        };

        if (player) {
            modalSettings.componentProps = { data: player };
        }

        const modal = await this.modal.create(modalSettings);
        await modal.present();
    }

    sort(): void {
        this.battleService.sortByInitiative();
    }

    reorder({detail}: any): void {
        Utils.moveInArray(this.battle.players, detail.from, detail.to);
        this.battleService.update({ players: this.battle.players });
        detail.complete();
    }

    deletePlayer(player: BattlePlayer): void {
        this.battleService.deletePlayer(player);
    }

    getColorForPlayer(player: BattlePlayer): string {
        return (player.hitPoints != null && player.hitPoints <= 0) ? 'light' : '';
    }

}
