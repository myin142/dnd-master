import { Component } from '@angular/core';
import { Spell } from '../../shared/dnd.model';
import { ModalController } from '@ionic/angular';
import { SpellDetailComponent } from '../../modules/spells/spell-detail/spell-detail.component';

@Component({
    selector: 'app-spells-page',
    templateUrl: './spells.page.html',
})
export class SpellsPage {
    name: string;

    constructor(private modal: ModalController) {
    }

    async openDetail(spell: Spell): Promise<void> {
        const modal = await this.modal.create({
            component: SpellDetailComponent,
            componentProps: { spell },
        });

        await modal.present();
    }
}
