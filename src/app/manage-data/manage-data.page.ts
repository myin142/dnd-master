import { Component } from '@angular/core';
import { MenuItem } from '../modules/menu/menu-list/menu-list.component';
import { DndDatabaseService } from '../services/dnd-database.service';
import { ToastController } from '@ionic/angular';

@Component({
    selector: 'app-manage-data',
    templateUrl: './manage-data.page.html',
    styleUrls: ['./manage-data.page.scss'],
})
export class ManageDataPage {

    dataItems: MenuItem[] = [
        {label: 'Spells', link: 'spells'},
    ];

    dbItems: MenuItem[] = [
        {label: 'Upload Data', link: 'upload'},
    ];

    constructor(private dndDatabase: DndDatabaseService,
                private toast: ToastController) {
    }

    async reInitDatabase(): Promise<void> {
        await this.dndDatabase.initDatabase(true);
        const toast = await this.toast.create({
            message: 'Database cleared',
            duration: 1000,
            buttons: [
                {text: 'Close', role: 'cancel'},
            ],
        });
        return toast.present();
    }

}
