import { Component, OnInit } from '@angular/core';
import { DndDatabaseService } from '../../services/dnd-database.service';

@Component({
    selector: 'app-upload',
    templateUrl: './upload.page.html',
    styleUrls: ['./upload.page.scss'],
})
export class UploadPage implements OnInit {

    constructor(private dndDatabase: DndDatabaseService) {
    }

    ngOnInit() {
    }

    import({target}): void {
        this.dndDatabase.importFromFile(target.files[0]);
    }

}
