import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ManageDataPage } from './manage-data.page';
import { MenuModule } from '../modules/menu/menu.module';
import { UploadPage } from './upload/upload.page';
import { SpellsPage } from './spells/spells.page';
import { SpellsModule } from '../modules/spells/spells.module';

const routes: Routes = [
    {path: '', component: ManageDataPage},
    {path: 'upload', component: UploadPage},
    {path: 'spells', component: SpellsPage},
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        MenuModule,
        SpellsModule,
    ],
    declarations: [
        ManageDataPage,
        UploadPage,
        SpellsPage,
    ],
})
export class ManageDataPageModule {
}
