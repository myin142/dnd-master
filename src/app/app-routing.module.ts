import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {path: '', redirectTo: 'battle', pathMatch: 'full'},
    {path: 'battle', loadChildren: () => import('./battle/battle.module').then(m => m.BattlePageModule)},
    {path: 'manage-data', loadChildren: () => import('./manage-data/manage-data.module').then(m => m.ManageDataPageModule)},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules}),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
