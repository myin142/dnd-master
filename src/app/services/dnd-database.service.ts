import { Injectable } from '@angular/core';
import { DatabaseService } from './database/database.service';
import { ColumnType } from './database/database.model';

@Injectable({
    providedIn: 'root',
})
export class DndDatabaseService {

    constructor(private database: DatabaseService) {
    }

    async initDatabase(overwrite = false): Promise<void> {
        await this.database.createDatabase({
            name: 'dnd-data',
            location: 'default',
        });

        await this.database.createTable({
            dndClasses: {
                name: { type: ColumnType.String, primary: true },
                hitDice: ColumnType.SmallInteger,
            },
            dndSubclasses: {
                name: { type: ColumnType.String, primary: true },
                class: { table: 'dndClasses', column: 'name' },
            },
            monsters: {
                id: { type: ColumnType.Integer, primary: true },
                name: ColumnType.String,
                armor: ColumnType.Integer,
                hitPoints: ColumnType.Integer,
                hitDice: ColumnType.String,
            },
            spells: {
                id: { type: ColumnType.Integer, primary: true },
                name: ColumnType.String,
                desc: ColumnType.Text,
                higherLevel: ColumnType.Text,
                range: ColumnType.Integer, // in feet
                components: ColumnType.String,
                material: ColumnType.String,
                duration: ColumnType.String,
                castingTime: ColumnType.String,
                level: ColumnType.Integer,
            }
        }, overwrite);
    }

    async importFromFile(file: File): Promise<void> {
        const reader = new FileReader();
        reader.onload = this.uploadData.bind(this);
        reader.readAsText(file);
    }

    async select<T>(table: string, query: object): Promise<T[]> {
        return this.database.select<T>(table, query);
    }

    private async uploadData({target}): Promise<void> {
        const data = JSON.parse(target.result);
        await Promise.all(Object.keys(data).map(async table => {
            await this.database.batchInsert(table, data[table]);
        }));
    }

}
