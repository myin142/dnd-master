import { HttpService } from './http.service';
import { Observable } from 'rxjs';

export abstract class BaseHttpService {

    protected abstract BASE_URL: string;

    constructor(protected http: HttpService) {
    }

    private prependBaseUrl(url: string): string {
        if (this.isAbsoluteUrl(url)) {
            return url;
        }

        if (url.startsWith('/')) {
            url = url.slice(1, url.length);
        }

        return `${this.BASE_URL}/${url}`;
    }

    private isAbsoluteUrl(url: string): boolean {
        return url.startsWith('http://') || url.startsWith('https://');
    }

    public getCached<T>(url: string): Observable<T> {
        return this.http.getCached(this.prependBaseUrl(url));
    }

    public get<T>(url: string): Observable<T> {
        return this.http.get(this.prependBaseUrl(url));
    }

}
