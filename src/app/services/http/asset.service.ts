import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';

@Injectable({
    providedIn: 'root',
})
export class AssetService extends BaseHttpService {
    protected BASE_URL = `/assets`;
}
