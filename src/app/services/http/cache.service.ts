import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class CacheService {

    private cache: { [key: string]: Observable<any> } = {};

    cacheRequest<T>(key: string, request: Observable<T>, timeToLive?: number): Observable<T> {
        if (this.cache[key] == null) {
            this.cache[key] = request.pipe(shareReplay(1, timeToLive));
        }

        return this.cache[key];
    }

    clearCache(key?: string): void {
        if (key != null) {
            this.cache[key] = null;
        } else {
            this.cache = {};
        }
    }
}
