import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CacheService } from './cache.service';

@Injectable({
    providedIn: 'root',
})
export class HttpService {

    constructor(private http: HttpClient,
                private cache: CacheService) {
    }

    get<T>(url: string): Observable<T> {
        return this.request({method: 'GET', url});
    }

    getCached<T>(url: string): Observable<T> {
        return this.getCachedRequest(url, this.get(url));
    }

    getCachedRequest<T>(url: string, request: Observable<T>): Observable<T> {
        return this.cache.cacheRequest(url, request);
    }

    private request(req: HttpRequest): Observable<any> {
        return this.http.request(req.method, req.url);
    }

}

export interface HttpRequest {
    url: string;
    method: string;
    body?: any;
}
