export enum ColumnType {
    String = 'VARCHAR',
    Integer = 'INTEGER',
    Text = 'TEXT',
    SmallInteger = 'TINYINT',
}

export interface Rows {
    length: number;
    item(n: number): any;
}

export interface TableOptions {
    [table: string]: {
        [col: string]: ColumnType | ColumnDetails | TableLink;
    };
}

export interface TableLink {
    table: string;
    column: string;
}

export interface ColumnDetails {
    type: ColumnType;
    primary?: boolean;
    unique?: boolean;
    required?: boolean;
}
