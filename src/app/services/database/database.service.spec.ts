import { async, TestBed } from '@angular/core/testing';
import { DatabaseService } from './database.service';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { RegexBuilder } from '../../util/regex-builder';
import { ColumnType } from './database.model';

describe('DatabaseService', () => {

    const database: SQLiteObject = jasmine.createSpyObj('SQLiteObject', {executeSql: {rows: []}});
    const sqlite: SQLite = jasmine.createSpyObj('SQLite', {create: database});

    let service: DatabaseService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            providers: [
                DatabaseService,
                {provide: SQLite, useValue: sqlite},
            ],
        });

        service = TestBed.get(DatabaseService);
        service.createDatabase({name: ''});
    }));

    it('should create database', () => {
        expect(sqlite.create).toHaveBeenCalled();
    });

    describe('Create Table', () => {
        it('should drop table on overwrite', async () => {
            await service.createTable({table: {key: ColumnType.Integer}}, true);

            expect(database.executeSql).toHaveBeenCalledWith(
                jasmine.stringMatching(RegexBuilder.for(`DROP TABLE * table`).build()),
                jasmine.anything(),
            );
        });

        it('should create table', async () => {
            await service.createTable({
                table: {key: ColumnType.Integer},
            });

            expect(database.executeSql).toHaveBeenCalledWith(
                jasmine.stringMatching(RegexBuilder.for(`CREATE TABLE * table`).build()),
                jasmine.anything(),
            );
        });

        it('should create table with columns', async () => {
            await service.createTable({
                table: {column: ColumnType.Integer, name: ColumnType.String},
            });

            expect(database.executeSql).toHaveBeenCalledWith(
                jasmine.stringMatching(RegexBuilder
                    .for(`table ( column ${ColumnType.Integer} , name ${ColumnType.String} ) `)
                    .ignoreSpace().build()),
                jasmine.anything(),
            );
        });

        it('should create table with primary key', async () => {
            await service.createTable({
                table: {column: {type: ColumnType.Integer, primary: true}},
            });

            expect(database.executeSql).toHaveBeenCalledWith(
                jasmine.stringMatching(RegexBuilder
                    .for(`( column ? PRIMARY KEY )`)
                    .ignoreSpace().build()),
                jasmine.anything(),
            );
        });

        it('should create table with unique column', async () => {
            await service.createTable({
                table: {column: {type: ColumnType.Integer, unique: true}},
            });

            expect(database.executeSql).toHaveBeenCalledWith(
                jasmine.stringMatching(RegexBuilder
                    .for(`( column ? UNIQUE )`)
                    .ignoreSpace().build()),
                jasmine.anything(),
            );
        });

        it('should create table with not null column', async () => {
            await service.createTable({
                table: {column: {type: ColumnType.Integer, required: true}},
            });

            expect(database.executeSql).toHaveBeenCalledWith(
                jasmine.stringMatching(RegexBuilder
                    .for(`( column ? NOT NULL )`)
                    .ignoreSpace().build()),
                jasmine.anything(),
            );
        });

        it('should create table foreign key column of same type only', async () => {
            await service.createTable({
                foreign: {key: { type: ColumnType.Integer, unique: true }},
                table: {ref: {table: 'foreign', column: 'key'}},
            });

            expect(database.executeSql).toHaveBeenCalledWith(
                jasmine.stringMatching(RegexBuilder
                    .for(`( ref ${ColumnType.Integer} , * )`)
                    .ignoreSpace().build()),
                jasmine.anything(),
            );
        });

        it('should create table with foreign key constraint', async () => {
            await service.createTable({
                foreign: {key: ColumnType.Integer},
                table: {ref: {table: 'foreign', column: 'key'}},
            });

            expect(database.executeSql).toHaveBeenCalledWith(
                jasmine.stringMatching(RegexBuilder
                    .for(`( * FOREIGN KEY ( ref ) REFERENCES foreign ( key ) )`)
                    .ignoreSpace().build()),
                jasmine.anything(),
            );
        });
    });

});
