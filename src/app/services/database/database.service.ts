import { SQLite, SQLiteDatabaseConfig, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Injectable } from '@angular/core';
import { Optional } from '../../util/optional';
import { ColumnDetails, ColumnType, Rows, TableLink, TableOptions } from './database.model';
import { TypeCheck } from '../../util/type-check';

@Injectable({
    providedIn: 'root',
})
export class DatabaseService {

    private database: SQLiteObject;

    constructor(private sqlite: SQLite) {
    }

    async createDatabase(dbOptions: SQLiteDatabaseConfig): Promise<void> {
        this.database = await this.sqlite.create(dbOptions);
    }

    async createTable(tables: TableOptions, overwrite = false): Promise<void> {
        this.assertDatabaseOpen();

        await Promise.all(Object.keys(tables).map(async table => {
            if (overwrite) {
                await this.database.executeSql(`DROP TABLE IF EXISTS ${table}`, []);
                console.log(`DROP TABLE ${table}`);
            }

            const constraints = [];
            const columns = tables[table];
            const columnString = Object.keys(columns).map(col => {

                let value = columns[col] as any;
                if (TypeCheck.hasKey('table', value)) {
                    const tableLink = value as TableLink;
                    const foreignTable = tables[tableLink.table];
                    if (foreignTable) {
                        throw new Error('References to invalid table');
                    }

                    const foreignCol = foreignTable[tableLink.column];
                    if (foreignCol) {
                        throw new Error(`References to invalid column in table ${tableLink.table}`);
                    }

                    if (TypeCheck.isString(foreignCol)) {
                        value = this.getColumnDefinition(foreignCol);
                    } else {
                        // Can use ColumnDetails since another TableLink is not supported
                        const objCol = foreignCol as ColumnDetails;
                        value = this.getColumnDefinition(objCol.type);
                    }
                    constraints.push(`FOREIGN KEY(${col}) REFERENCES ${tableLink.table}(${tableLink.column})`);
                } else {
                    value = this.getColumnDefinition(value);
                }

                return `${col} ${value}`;
            }).join(', ');
            await this.database.executeSql(`CREATE TABLE IF NOT EXISTS ${table}(${columnString}, ${constraints.join(',')})`, []);
            console.log(`CREATE TABLE ${table}`, columns);
        }));
    }

    private getColumnDefinition(col: ColumnType | ColumnDetails | TableLink): string {
        let value = col as any;
        if (TypeCheck.hasKey('type', value)) {
            value = this.columnDefForDetails(value as ColumnDetails);
        } else if (TypeCheck.hasKey('table', value)) {
            throw new Error('Do not support nested linked foreign keys');
        }

        return value;
    }

    private columnDefForDetails(col: ColumnDetails): string {
        let def = `${col.type}`;
        def += Optional.of(col.primary).returnIfTrue(' PRIMARY KEY', '');
        def += Optional.of(col.unique).returnIfTrue(' UNIQUE', '');
        def += Optional.of(col.required).returnIfTrue(' NOT NULL', '');

        return def;
    }

    async batchInsert(table: string, data: object[]): Promise<void> {
        this.assertDatabaseOpen();
        const inserts = data
            .map(d => this.createInsertStatement(table, d))
            .map(insert => ([insert.stmt, insert.params]));
        await this.database.sqlBatch(inserts);
        console.log(`INSERT BATCH ${table}`, data);
    }

    async insertData(table: string, data: object): Promise<void> {
        this.assertDatabaseOpen();
        const insert = this.createInsertStatement(table, data);
        await this.database.executeSql(insert.stmt, insert.params);
        console.log(`INSERT DATA ${table}`, data);
    }

    private createInsertStatement(table: string, data: object): { stmt: string, params: any[] } {
        const columns = Object.keys(data);
        const values = columns.map(col => data[col]);
        const parameters = this.parameterizeFor(values);

        return {
            stmt: `INSERT INTO ${table}(${columns.join(', ')}) VALUES(${parameters})`,
            params: values,
        };
    }

    async select<T>(table: string, conditions: object = {}): Promise<T[]> {
        this.assertDatabaseOpen();

        const values = [];
        const conditionArr = Object.keys(conditions).map(col => {
            const value = conditions[col];
            values.push(value);

            let condition = `${col}`;
            if (typeof value === 'string' && (value.startsWith('%') || value.endsWith('%'))) {
                condition += ` LIKE`;
            } else {
                condition += ` =`;
            }

            return `${condition} ?`;
        });

        let selectStr = `SELECT * FROM ${table}`;
        if (conditionArr.length > 0) {
            selectStr += ` WHERE ${conditionArr.join(' AND ')}`;
        }

        console.log(`SELECT ${table}`, conditions);
        return this.rowsToArray<T>((await this.database.executeSql(selectStr, values)).rows);
    }

    private rowsToArray<T>(rows: Rows): T[] {
        const result = [];
        for (let i = 0; i < rows.length; i++) {
            result.push(rows.item(i));
        }
        return result;
    }

    private parameterizeFor(arr: any[]): string {
        return arr.map(() => '?').join(',');
    }

    private assertDatabaseOpen(): void {
        if (this.database == null) {
            throw new Error('DatabaseService: database has to be opened first');
        }
    }

}

