import { WebServer, Response, Request } from '@ionic-native/web-server/ngx';
import { Zeroconf } from '@ionic-native/zeroconf/ngx';
import { isObservable, Observable } from 'rxjs';

export abstract class RestService {

    private static DEFAULT_HEADERS: { [key: string]: string } = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET',
    };

    protected abstract serverHost: string;
    protected abstract serverType: string;
    protected serverDomain = 'local.';
    protected serverPort = 8080;

    protected abstract webServer: WebServer;
    protected abstract zeroConf: Zeroconf;

    async start(): Promise<void> {
        await this.webServer.start(this.serverPort);
        console.log(`Server started at http://localhost:${this.serverPort}/`);

        const result = await this.zeroConf.register(
            this.serverType, this.serverDomain, this.serverHost, this.serverPort, {});

        console.log(`Registered Service`, result.service);
        this.initResponses();
    }

    async stop(): Promise<void> {
        await this.webServer.stop();
        console.log('Stopped WebServer');

        await this.zeroConf.stop();
        console.log('Stopped Discovery Service');
    }

    protected getQuery(request: Request): { [key: string]: string[] } {
        const queryParam = {};

        if (request.query) {
            request.query.split('&').map(paramPair => paramPair.split('=')).forEach(([name, value]) => {
                let query = queryParam[name];
                if (query == null) {
                    query = [];
                }

                query.push(value);
                queryParam[name] = query;
            });
        }

        return queryParam;
    }

    protected async getResponse(request: Request): Promise<Observable<any> | any> {
        return null;
    }

    private initResponses(): void {
        this.webServer.onRequest().subscribe(async request => {

            const response = await this.getResponse(request);
            if (isObservable(response)) {
                response.subscribe(data => {
                    this.sendResponse(request, data);
                });
            } else {
                this.sendResponse(request, response);
            }
        });
    }

    private async sendResponse({requestId}: Request, data: any) {
        const finalResponse: Response = {
            status: 404,
            body: `{ error: 'Not Found' }`,
            headers: RestService.DEFAULT_HEADERS,
        };

        if (data != null) {
            finalResponse.status = 200;
            finalResponse.body = data;
        }

        console.log({ finalResponse });
        await this.webServer.sendResponse(requestId, finalResponse);
    }

}
