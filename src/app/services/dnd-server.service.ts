import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { Request, WebServer } from '@ionic-native/web-server/ngx';
import { Zeroconf } from '@ionic-native/zeroconf/ngx';
import { DndDatabaseService } from './dnd-database.service';
import { Observable } from 'rxjs';
import { AssetService } from './http/asset.service';

@Injectable({
    providedIn: 'root',
})
export class DndServerService extends RestService {

    protected serverHost = 'DND-Master';
    protected serverType = '_dnd-service._tcp.';

    constructor(protected webServer: WebServer,
                protected zeroConf: Zeroconf,
                private asset: AssetService,
                private dndDatabase: DndDatabaseService) {
        super();
    }

    protected async getResponse(request: Request): Promise<Observable<any> | any> {
        console.log({ request });
        const query = super.getQuery(request);

        switch (request.path) {
            // case '/classes': return this.dndDatabase.getClasses();
            // case '/subclasses': return this.dndDatabase.getSubclassesForClass(query.class[0]);
            // case '/abilityScores': return this.asset.getCached(`ability-scores.json`);
        }
    }

}
