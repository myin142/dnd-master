import { NgModule } from '@angular/core';
import { SpellsService } from './spells.service';
import { SpellSearchComponent } from './spell-search/spell-search.component';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SpellDetailComponent } from './spell-detail/spell-detail.component';
import { MenuModule } from '../menu/menu.module';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        MenuModule,
    ],
    declarations: [
        SpellSearchComponent,
        SpellDetailComponent,
    ],
    entryComponents: [
        SpellDetailComponent,
    ],
    providers: [
        SpellsService,
    ],
    exports: [
        SpellSearchComponent,
    ],
})
export class SpellsModule {
}
