import { Component, OnInit } from '@angular/core';
import { SearchComponent } from '../../../shared/search.component';
import { Spell } from '../../../shared/dnd.model';
import { SpellsService } from '../spells.service';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, switchMap } from 'rxjs/operators';

@Component({
    selector: 'app-spell-search',
    templateUrl: './spell-search.component.html',
})
export class SpellSearchComponent extends SearchComponent<Spell, string> implements OnInit {

    spells: Observable<Spell[]>;

    constructor(private spellService: SpellsService) {
        super();
    }

    ngOnInit(): void {
        this.spells = this.searchRequest.pipe(
            debounceTime(500),
            distinctUntilChanged(),
            filter(x => x.length > 2),
            switchMap(name => this.spellService.searchSpells(name)),
        );
    }

}
