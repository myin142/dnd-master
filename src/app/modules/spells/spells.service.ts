import { Injectable } from '@angular/core';
import { DndDatabaseService } from '../../services/dnd-database.service';
import { from, Observable } from 'rxjs';
import { Spell } from '../../shared/dnd.model';

@Injectable()
export class SpellsService {

    constructor(private dndDatabase: DndDatabaseService) {
    }

    searchSpells(name: string): Observable<Spell[]> {
        return from(this.dndDatabase.select<Spell>('spells', { name: `%${name}%` }));
    }

}
