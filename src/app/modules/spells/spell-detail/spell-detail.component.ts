import { Component, Input } from '@angular/core';
import { Spell } from '../../../shared/dnd.model';

@Component({
    selector: 'app-spell-detail',
    templateUrl: './spell-detail.component.html',
})
export class SpellDetailComponent {

    @Input() spell: Spell;

    getData(): { name: string; value: any }[] {
        return [
            { name: 'Description', value: this.spell.desc },
            { name: 'Higher Level', value: this.spell.higherLevel },
            { name: 'Range', value: this.spell.range },
            { name: 'Components', value: this.spell.components },
            { name: 'Material', value: this.spell.material },
            { name: 'Duration', value: this.spell.duration },
            { name: 'Casting Time', value: this.spell.castingTime },
            { name: 'Level', value: this.spell.level },
        ];
    }

}
