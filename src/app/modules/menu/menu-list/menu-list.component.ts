import { Component, Input } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
    selector: 'app-menu-list',
    templateUrl: './menu-list.component.html',
})
export class MenuListComponent {
    @Input() menuItems: MenuItem[];

    constructor(public menu: MenuController) {
    }

}

export interface MenuItem {
    label: string;
    link: string;
}
