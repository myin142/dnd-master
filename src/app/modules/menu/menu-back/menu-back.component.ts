import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-menu-back',
    templateUrl: './menu-back.component.html',
})
export class MenuBackComponent {
    @Input() link = '../';
}
