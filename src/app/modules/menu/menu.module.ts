import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuCloseComponent } from './menu-close/menu-close.component';
import { IonicModule } from '@ionic/angular';
import { MenuHeaderComponent } from './menu-header/menu-header.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { RouterModule } from '@angular/router';
import { MenuBackComponent } from './menu-back/menu-back.component';

@NgModule({
    declarations: [
        MenuCloseComponent,
        MenuHeaderComponent,
        MenuListComponent,
        MenuBackComponent,
    ],
    exports: [
        MenuCloseComponent,
        MenuHeaderComponent,
        MenuListComponent,
        MenuBackComponent,
    ],
    imports: [
        CommonModule,
        IonicModule,
        RouterModule,
    ],
})
export class MenuModule {
}
