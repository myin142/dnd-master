import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonsterService } from './monster.service';
import { MonsterSearchComponent } from './monster-search/monster-search.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [
        MonsterSearchComponent,
    ],
    imports: [
        CommonModule,
        IonicModule,
    ],
    providers: [
        MonsterService,
    ],
    exports: [
        MonsterSearchComponent,
    ],
})
export class MonsterModule {
}
