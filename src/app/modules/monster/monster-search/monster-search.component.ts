import { Component, OnInit } from '@angular/core';
import { MonsterService } from '../monster.service';
import { Monster } from '../../../shared/dnd.model';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, switchMap } from 'rxjs/operators';
import { SearchComponent } from '../../../shared/search.component';

@Component({
    selector: 'app-monster-search',
    templateUrl: './monster-search.component.html',
})
export class MonsterSearchComponent extends SearchComponent<Monster, string> implements OnInit {

    monsters: Observable<Monster[]>;

    constructor(private monsterService: MonsterService) {
        super();
    }

    ngOnInit(): void {
        this.monsters = this.searchRequest.pipe(
            debounceTime(500),
            distinctUntilChanged(),
            filter(x => x.length > 2),
            switchMap(name => this.monsterService.searchMonster(name)),
        );
    }

}
