import { Injectable } from '@angular/core';
import { DndDatabaseService } from '../../services/dnd-database.service';
import { Monster } from '../../shared/dnd.model';
import { from, Observable } from 'rxjs';

@Injectable()
export class MonsterService {

    constructor(private dndDatabase: DndDatabaseService) {
    }

    searchMonster(name: string): Observable<Monster[]> {
        return from(this.dndDatabase.select<Monster>('monsters', { name: `%${name}%` }));
    }

}
