import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DndDatabaseService } from './services/dnd-database.service';
import { build } from '../environments/build-info';
import { MenuItem } from './modules/menu/menu-list/menu-list.component';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent {

    build = build;

    menuItems: MenuItem[] = [
        {label: 'Battle', link: 'battle'},
        {label: 'Manage Data', link: 'manage-data'},
    ];

    constructor(private platform: Platform,
                private splashScreen: SplashScreen,
                private statusBar: StatusBar,
                private dndDatabase: DndDatabaseService) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleLightContent();
            this.splashScreen.hide();
            this.dndDatabase.initDatabase();
        });
    }

}
