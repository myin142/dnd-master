export class TypeCheck {

    static hasKey(key: string, obj: any): boolean {
        return typeof obj === 'object' && key in obj;
    }

    static isString(value: any): boolean {
        return typeof value === 'string';
    }

}
