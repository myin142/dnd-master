import { RegexBuilder } from './regex-builder';

describe('RegexBuilder', () => {

    it('should match', () => {
        expect(RegexBuilder.for(`hello`)
            .build()
            .test(`I say hello to you`)).toBeTruthy();
    });

    it('should start with', () => {
        expect(RegexBuilder.for(`hello`)
            .startsWith()
            .build()
            .test(`I say hello to you`)).toBeFalsy();
    });

    it('should match insensitive', () => {
        expect(RegexBuilder.for(`hello`)
            .insensitive()
            .build()
            .test(`I say HELLO to you`)).toBeTruthy();
    });

    it('should ignore spaces', () => {
        expect(RegexBuilder.for(`hello world`)
            .ignoreSpace()
            .build()
            .test('helloworld')).toBeTruthy();
    });

    it('should match anything', () => {
        expect(RegexBuilder.for(`Hello * World`)
            .build()
            .test('Hello my beautiful World')).toBeTruthy();
    });

    it('should ignore spaces and match anything', () => {
        expect(RegexBuilder.for(`Hello World * How * doing`)
            .ignoreSpace()
            .build()
            .test('Hello    World. How well are you doing?')).toBeTruthy();
    });

    it('should match one word', () => {
        expect(RegexBuilder.for(`Hello ? World`)
            .build()
            .test('Hello wonderful World')).toBeTruthy();
    });

});
