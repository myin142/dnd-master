/**
 * Supported Special Expressions:
 *      * - matches anything
 *      ? - matches one word
 */
export class RegexBuilder {

    private static ESCAPE_CHARACTERS = ['(', ')', '^', '$'];
    private static ANYTHING_REGEX = '(?:.*)';
    private static WORD_REGEX = '\\w+';
    private static SPACE_REGEX = '\\s*';

    private static SPECIAL_EXP: SpecialExpression[] = [
        { char: '*', regex: RegexBuilder.ANYTHING_REGEX },
        { char: '?', regex: RegexBuilder.WORD_REGEX },
    ];

    private options: RegexOptions = {};

    constructor(private regex: string = '') {
    }

    private static escapeCharacters(str: string): string {
        RegexBuilder.ESCAPE_CHARACTERS.forEach(char => {
            str = str.replace(new RegExp(`\\${char}`, 'g'), `\\${char}`);
        });

        return str;
    }

    static for(regexStr: string): RegexBuilder {
        return new RegexBuilder(regexStr);
    }

    ignoreSpace(): RegexBuilder {
        this.options.ignoreSpace = true;
        return this;
    }

    startsWith(): RegexBuilder {
        this.options.startsWith = true;
        return this;
    }

    insensitive(): RegexBuilder {
        this.options.insensitive = true;
        return this;
    }

    build(): RegExp {
        return new RegExp(this.regexWithOptions, this.flagFromOptions);
    }

    private regexSplit(str: string[], exps: SpecialExpression[]): string[] {
        return str.map(x => {
            if (exps.length === 0) {
                return RegexBuilder.escapeCharacters(x);
            }

            const exp = exps.shift();
            return this.regexSplit(x.split(exp.char), [ ...exps ]).join(exp.regex);
        });
    }

    private get regexWithOptions(): string {
        let result = this.regexSplit([this.regex], [ ...RegexBuilder.SPECIAL_EXP ])[0];

        if (this.options.ignoreSpace) {
            result = result.replace(/\s+/g, RegexBuilder.SPACE_REGEX);
        }

        if (this.options.startsWith) {
            result = `^${result}`;
        }
        return result;
    }

    private get flagFromOptions(): string {
        let result = '';
        if (this.options.insensitive) {
            result += 'i';
        }
        return result;
    }

}

export class RegexOptions {
    startsWith?: boolean;
    insensitive?: boolean;
    ignoreSpace?: boolean;
}

export class SpecialExpression {
    char: string;
    regex: string;
}
