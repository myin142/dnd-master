export class Optional<T> {

    static of<T>(value: T): Optional<T> {
        return new Optional<T>(value);
    }

    constructor(private data: T) {
    }

    filter(filterFn: (data: T) => boolean): Optional<T> {
        const data = filterFn(this.data) ? this.data : null;
        return new Optional<T>(data);
    }

    map<R>(mapFn: (data: T) => R): Optional<R> {
        return new Optional<R>(mapFn(this.data));
    }

    orElse(elseValue: T): T {
        return (this.data != null) ? this.data : elseValue;
    }

    ifPresent(fn: (data: T) => void): void {
        if (this.data != null) {
            fn(this.data);
        }
    }

    returnIfTrue<R>(value: R, elseValue: R = null): R {
        if (this.isTrue()) {
            return value;
        }

        return elseValue;
    }

    private isTrue(): boolean {
        return typeof this.data === 'boolean' && this.data === true;
    }

}
