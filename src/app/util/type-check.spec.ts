import { TypeCheck } from './type-check';

describe('TypeCheck', () => {

    it('should check if object has key', () => {
        expect(TypeCheck.hasKey('key', { key: '' })).toBeTruthy();
    });

    it('should check if string', () => {
        expect(TypeCheck.isString('')).toBeTruthy();
    });

});
