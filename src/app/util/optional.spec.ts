import { Optional } from './optional';

describe('Optional', () => {

    describe('ReturnIfTrue', () => {
        it('should return value if true', () => {
            expect(Optional.of(true).returnIfTrue('True')).toEqual('True');
        });

        it('should return else value if false', () => {
            expect(Optional.of(false).returnIfTrue('True', 'False')).toEqual('False');
        });
    });

});
