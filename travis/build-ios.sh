#!/bin/bash -v

set -e

# Fix error where it always uses version 2.0.1 but needs 2.0.2
ionic cordova plugin add cordova-plugin-add-swift-support

# Build Ionic App for Android
ionic cordova platform add ios --nofetch
ionic cordova build ios --prod --release
