#!/bin/bash -v

set -e

mkdir -p output/
mkdir -p Payload/

cp -R platforms/ios/build/emulator/DND-Master.app Payload/
zip -0 -y -r output/ios-release-unsigned.ipa Payload/

tar zcvf output/ios-release-unsigned.app.tgz Payload/DND-Master.app
