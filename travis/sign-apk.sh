#!/bin/bash

if [[ -z $1 ]]; then
	echo "No store password";
	exit 1;
fi

APK="output/android-release-unsigned.apk"

jarsigner -verbose \
	-sigalg SHA1withRSA \
	-digestalg SHA1 \
	-keystore release-key.keystore \
	-storepass "$1" \
	"$APK" \
	release-key

jarsigner --verify "$APK"
"${ANDROID_HOME}"/build-tools/"${ANDROID_VERSION}"/zipalign -v 4 "$APK" output/android-release.apk
